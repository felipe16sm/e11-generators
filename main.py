# Não altere a linha seguinte
import time

# Altere a linha seguinte adicionando zeros conforme o video do enunciado
MAX_RANGE = 10000000

def exercise_1(max_range):
    return [num for num in range(0, max_range)]


def exercise_1_yield(max_range):
   i=0
   while i < max_range:
       yield i
       i += 1




# Nao altere a partir dessa linha para baixo !!

# Sem YIELD
start = time.process_time()

exercise_1(MAX_RANGE)

end = time.process_time() - start
print(f"-- Tempo de execução SEM YIELD: {end:.6f} segundos --")


# Com YIELD
start = time.process_time()

exercise_1_yield(MAX_RANGE)

end = time.process_time() - start
print(f"-- Tempo de execução COM YIELD: {end:.6f} segundos --")